var mongoose = require('mongoose');
var Schema = mongoose.Schema;



module.exports = mongoose.model('Recipe', new Schema({
  date_created: {
    type: String,
    default: Date.now
  },
  status: String,
  title: String,
  image: String,
  noOfViews: Number,
  categories: { type:String },
  // author: [{
  //   name:{
  //     type:String
  //   },
  //   userId:{
  //     type:String
  //   }
  // }],
  author: String,
  slug: String,


  // numberServings: {type:Number},
  cookingSteps: [{
    no:{
      type:Number
    },
    description:{
      type:String
    },
    ingredients:[{
      item:{type:String},
      qty: {type:String}
    }],
    time: Number,
    img: String,
    note: String

  }]


  
}));