var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// module.exports = mongoose.model('SubCategory' = new Schema({
//     Name: {type: String}
//     }));

// module.exports = mongoose.model('Category' = new Schema({
// Category_title: String
// , Category_description: String
// , Category_banner: String
// , Recipes: [RecipeSchema]
// }));

var SubCategorySchema = mongoose.Schema({
    Name: {type: String}
    });

var CategorySchema = mongoose.Schema({
Category_title: String,
Category_description: String,
Category_banner: String,
SubCategory: [SubCategorySchema],
//Recipes: [RecipeSchema]
});

var category = mongoose.model('Category',CategorySchema);
module.exports = category;