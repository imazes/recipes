
// =================================================================
// get the packages we need ========================================
// =================================================================
var express   = require('express');
var app         = express();
var bodyParser  = require('body-parser');
var morgan      = require('morgan');
var mongoose    = require('mongoose');

var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var bcrypt = require('bcryptjs');

var config = require('./config'); // get our config file
var User   = require('./app/models/user'); // get our mongoose model
var Recipe   = require('./app/models/recipe'); // get our mongoose model
var category = require('./app/models/category'); //get our mongoose model

// =================================================================
// configuration ===================================================
// =================================================================
var port = process.env.PORT || 8080; // used to create, sign, and verify tokens
mongoose.connect(config.database); // connect to database
app.set('superSecret', config.secret); // secret variable

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// use morgan to log requests to the console
app.use(morgan('dev'));

// =================================================================
// routes ==========================================================
// =================================================================

app.post('/signup', function(req, res) {
  console.log(req.body);
  var hashedPassword = bcrypt.hashSync(req.body.password, 8);
  User.create({
    name : req.body.name,
    email : req.body.email,
    password: req.body.password
    // password : hashedPassword
  },
  function (err, user) {
    if (err) return res.status(500).send("There was a problem registering the user.")
    // create a token
    var token = jwt.sign({ id: user._id }, config.secret, {
      expiresIn: 86400 // expires in 24 hours
    });
    res.status(200).send({ auth: true, token: token });
  }); 

});


// basic route (http://localhost:8080)
app.get('/', function(req, res) {
  res.send('Hello! The API is at http://localhost:' + port + '/api');
});

app.get('/all', function(req, res) {
  User.find({}, function(err, users) {
    res.json(users);
  });
});

app.get('/recipes', function(req, res) {
  Recipe.find({}, function(err, recipes) {
    res.json(recipes);
  });
});

app.get('/recipes/recent/:limit/:skip', function(req, res) {

  var limit = req.params.limit
  var skip = req.params.skip;  

  Recipe.find({})
  // .select('title')
  .limit(parseInt(limit))
  .skip(parseInt(skip))
  .sort([['date_created', -1]])
  .exec(function(err, recipes) { 
    res.json(recipes); 
  });

  
});
app.get('/recipes/recent/', function(req, res) {

  
  Recipe.find({})
  // .select('title') 
  .sort([['date_created', -1]])
  .exec(function(err, recipes) { 
    res.json(recipes); 
  });

  
});

app.get('/recipes/popular', function(req, res) {

  Recipe.find({})
  .sort([['noOfViews', -1]])
  .exec(function(err, recipes) { 
    res.json(recipes); 
  });
  
});
app.get('/recipes/popular/:limit/:skip', function(req, res) {
  var limit = req.params.limit
  var skip = req.params.skip;  

  Recipe.find({})
  .limit(parseInt(limit))
  .skip(parseInt(skip))
  .sort([['noOfViews', -1]])
  .exec(function(err, recipes) { 
    res.json(recipes); 
  });
  
});

app.get('/category/:category_name/:limit/:skip',function(req, res) {
  var category_name = req.params.category_name;
  var limit = req.params.limit
  var skip = req.params.skip;  
  Recipe.find({
    categories: category_name
  })
  .limit(parseInt(limit))
  .skip(parseInt(skip))
  .exec(function(err, recipes) { res.json(recipes); });

});

app.get('/category/:category_name',function(req, res) {
  var category_name = req.params.category_name;  
  Recipe.find({
    categories: category_name
  })  
  .exec(function(err, recipes) { res.json(recipes); });

});

app.get('/recipe/:slug', function(req, res) {
  var slug = req.params.slug 

  Recipe.findOne({
    slug: slug
  }) 
  .exec(function(err, recipes) { 
    res.json(recipes); 
  });
  
});

// TODO: upsert incrementing no of views
app.get('/recipe/:slug/viewed', function(req, res) {
  var slug = req.params.slug 
  var query = {'slug':slug};
  // req.newData.slug = req.user.username;
  // MyModel.findOneAndUpdate(query, {$inc : {'post.likes' : 1}}, {upsert:true}, function(err, doc){
  //     if (err) return res.send(500, { error: err });
  //     return res.send("succesfully saved");
  // });

  // Recipe.findOneAndUpdate(query, {$inc : {'noOfViews' : 1}}).exec(function(err, recipes) { 
  //   // if (err) return res.send(500, { error: err });
  //   res.json(recipes); 
  // });

  Recipe.findOneAndUpdate({
    query: { slug: slug },
    update: { $inc: { noOfViews: 1 } },
    upsert: true
  })  
  .exec(function(err, recipes) { 
    if (err) return res.send(500, { error: err });
    res.json(recipes); 
  });
  
});



// ---------------------------------------------------------
// get an instance of the router for api routes
// ---------------------------------------------------------
var apiRoutes = express.Router();

// ---------------------------------------------------------
// authentication (no middleware necessary since this isnt authenticated)
// ---------------------------------------------------------
// http://localhost:8080/api/authenticate
apiRoutes.post('/login', function(req, res) {

  // find the user

  // return
  console.log(req.body.email);
  console.log(req.body.password);

  User.findOne({
    email: req.body.email
  }, function(err, user) {

    // console.log(user)
    // console.log(err)
    // console.log(user.password)
    // console.log(req.body.password)

    if (err) throw err;

    if (!user) {
      res.json({ success: false, message: 'Authentication failed. User not found.' });
    } else if (user) {
      // var hashedPassword = bcrypt.hashSync(req.body.password, 8);
      console.log(bcrypt.hashSync(req.body.password, 8));
      console.log(user.password);

      // check if password matches
      if (user.password != req.body.password) {
      // if (user.password != bcrypt.hashSync(req.body.password, 8)) {
        res.json({ success: false, message: 'Authentication failed. Wrong password.' });
      } else {

        // if user is found and password is right
        // create a token
        var payload = {
          admin: user.admin
        }
        var token = jwt.sign(payload, app.get('superSecret'), {
          expiresIn: 86400 // expires in 24 hours
        });

        res.json({
          success: true,
          message: 'Enjoy your token!',
          token: token
        });
      }

    }

  });
});

// ---------------------------------------------------------
// route middleware to authenticate and check token
// ---------------------------------------------------------
apiRoutes.use(function(req, res, next) {
  // console.log(req.headers);

  var token = req.headers['x-access-token'];
  if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
  
  jwt.verify(token, config.secret, function(err, decoded) {
    if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
    // res.status(200).send(decoded);
    next();
  });

  // // check header or url parameters or post parameters for token
  // // console.log(req.body);
  // // console.log(req.headers);
  // var token = req.body.token || req.param('token') || req.headers['authorization'];
  // // var token = req.body.token || req.param('token') || req.headers['x-access-token'];

  // console.log(token);

  // // decode token
  // if (token) {

  //   // verifies secret and checks exp
  //   jwt.verify(token, app.get('superSecret'), function(err, decoded) {
  //     if (err) {
  //       return res.json({ success: false, message: 'Failed to authenticate token.' });
  //     } else {
  //       // if everything is good, save to request for use in other routes
  //       req.decoded = decoded;
  //       next();
  //     }
  //   });

  // } else {

  //   // if there is no token
  //   // return an error
  //   return res.status(403).send({
  //     success: false,
  //     message: 'No token provided.'
  //   });

  // }

});

// ---------------------------------------------------------
// authenticated routes
// ---------------------------------------------------------



apiRoutes.post('/recipe/add', function(req, res) {

  recipe_data = req.body;

  var title = req.body.title;
  var slug = title.replace(/\s+/g, '-').toLowerCase();
  // var categories = 'testing';
  var categories = req.body.categories;
  // var author = '';
  var numberServings = req.body.numberServings;
  var cookingSteps = req.body.cookingSteps;

  Recipe.create({
    title: title,
    categories: categories,
    // author: author,
    slug: slug,
    numberServings: numberServings,
    cookingSteps: cookingSteps
  },
  // Recipe.create(recipe_data, 
    function (err, recipe) {
    if (err) return res.status(500).send("There was a problem adding recipe.")

    console.log(recipe);
    // create a token
    // var token = jwt.sign({ id: recipe._id }, config.secret, {
    //   expiresIn: 86400 // expires in 24 hours
    // });
    // res.status(200);

    res.status(200).send('For testing');
  }); 

});


apiRoutes.post('/recipe/addTest', function(req, res) {

  recipe_data = req.body;

  var title = 'asdasd asdas dasdas';
  // var title = req.body.title;
  slug = title.replace(/\s+/g, '-').toLowerCase();

  console.log(slug);
  
  let i=1;
  let unique = false
  let new_slug = slug;

  while ( !unique ) {
      // add one fish for each iteration
      Recipe.findOne({slug : slug}, function (err, exist) {
          if (exist){
            new_slug = slug +"-" + i;               
            unique = false
          }else{
            unique=true
          }
          i++;
      });

  }

//   Recipe.findOne({slug : slug}, function (err, exist) {
//     if (exist){
//        console.log('exist');        
//       }else{
//         console.log('not exist');      
//     }
//     // i++;
// });


    
  // console.log(new_slug);

  res.status(200).send('For testing');

  /*

  Recipe.find({slug : slug}, function (err, docs) {
      if (docs.length){
          cb('Name exists already',null);
      }else{
          user.save(function(err){
              cb(err,user);
          });
      }
  });
  */



  // var categories = 'testing';
  // var categories = req.body.categories;
  // // var author = '';
  // var numberServings = req.body.numberServings;
  // var cookingSteps = req.body.cookingSteps;

  // // console.log(cookingSteps);
  // // res.status(200).send('Testing');;


  
  // Recipe.create({
  //   title: title,
  //   categories: categories,
  //   // author: author,
  //   numberServings: numberServings,
  //   cookingSteps: cookingSteps
  // },function (err, recipe) {
  //   if (err) return res.status(500).send("There was a problem adding recipe.")

  //   console.log(recipe);
  //   // create a token
  //   // var token = jwt.sign({ id: recipe._id }, config.secret, {
  //   //   expiresIn: 86400 // expires in 24 hours
  //   // });
  //   // res.status(200);

  //   res.status(200).send('For testing');
  // }); 

});




apiRoutes.get('/', function(req, res) {
  res.json({ message: 'Welcome to the coolest API on earth!' });
});

apiRoutes.post('/users', function(req, res) {
  User.find({}, function(err, users) {
    res.json(users);
  });
});

apiRoutes.get('/check', function(req, res) {

  // var token = req.headers['x-access-token'];
  // if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
  
  // jwt.verify(token, config.secret, function(err, decoded) {
  //   if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
  //   res.status(200).send(decoded);
  // });

  res.json(req.decoded);
});

app.use('/api', apiRoutes);

// =================================================================
// start the server ================================================
// =================================================================
app.listen(port);
console.log('Cooking happens at http://localhost:' + port);
