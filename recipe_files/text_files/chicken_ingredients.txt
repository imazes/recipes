3 -4 lbs pork (cut in 2-inch cubes) or 3 -4 lbs chicken (cut into serving pieces)
1/2 cup vinegar
1/2 cup soy sauce
1 cup water
2 -3 bay leaves, crumbled
2 teaspoons peppercorns (whole)
4 garlic cloves, crushed
1 medium onion, chopped
3/4 teaspoon ground pepper
2 teaspoons salt (optional, I do not put it in, it is what the original cook uses)
